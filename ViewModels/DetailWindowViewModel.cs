﻿using BLToolkit.Data.Linq;
using CommunityToolkit.Mvvm.Input;
using LanitTest.Models;
using LanitTest.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace LanitTest.ViewModels
{
    /// <summary>
    /// Класс редактрования сотрудника
    /// </summary>
    public class DetailWindowViewModel : BaseViewModel
    {
        /// <summary>
        /// Сотрудник
        /// </summary>
        public Employee Employee { get; set; }

        private int jobIndex;
        /// <summary>
        /// Выбранный индекс должности
        /// </summary>
        public int JobIndex
        {
            get => jobIndex;
            set
            {
                SetProperty(ref jobIndex, value);
            }
        }
        
        private int subdivisionIndex;
        /// <summary>
        /// Выбранный индекс подразделения
        /// </summary>
        public int SubdivisionIndex
        {
            get => subdivisionIndex;
            set
            {
                SetProperty(ref subdivisionIndex, value);
            }
        }

        private List<JobTitle> jobTitles;
        /// <summary>
        /// Колекция должностей
        /// </summary>
        public List<JobTitle> JobTitles
        {
            get => jobTitles;
            set
            {
                SetProperty(ref jobTitles, value);
            }
        }

        private List<Subdivision> subdivisions;
        /// <summary>
        /// Колекция подраделений
        /// </summary>
        public List<Subdivision> Subdivisions
        {
            get => subdivisions;
            set
            {
                SetProperty(ref subdivisions, value);
            }
        }

        /// <summary>
        /// Команда закрыия окна
        /// </summary>
        public RelayCommand CancelCommand { get; set; }
        /// <summary>
        /// Команда сохранения сотрудника
        /// </summary>
        public RelayCommand SaveCommand { get; }

        /// <summary>
        /// Класс редактрования сотрудника
        /// </summary>
        /// <param name="contextDB"></param>
        public DetailWindowViewModel(ContextDB contextDB)
        {
            ContextDB = contextDB;

            var worker = new BackgroundWorker();
            worker.DoWork += (sender, e) =>
            {
                try
                {
                    JobTitles = ContextDB.JobTitle.ToList();
                    Subdivisions = ContextDB.Subdivision.ToList();
                    JobIndex = JobTitles.Select((j, index) => (j, index)).First(x => x.j.Id == Employee.JobTitle.Id).index;
                    SubdivisionIndex = Subdivisions.Select((s, index) => (s, index)).First(x => x.s.Id == Employee.Subdivision.Id).index;
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"{ex.Message}\n\n{ex.StackTrace}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            };
            worker.RunWorkerAsync();

            SaveCommand = new RelayCommand(Save);
        }

        /// <summary>
        /// Сохраняет изменения сотрудника
        /// </summary>
        private void Save()
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (sender, e) =>
            {
                try
                {
                    ContextDB.UpdateEmployee(Employee);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"{ex.Message}\n\n{ex.StackTrace}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            };
            worker.RunWorkerCompleted += (sender, e) => CancelCommand.Execute(null);
            worker.RunWorkerAsync();
        }
    }
}
