﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using LanitTest.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LanitTest.ViewModels
{
    /// <summary>
    /// Базовая ViewModel
    /// </summary>
    public class BaseViewModel : ObservableObject
    {
        /// <summary>
        /// Контекст базы данных
        /// </summary>
        protected ContextDB ContextDB { get; set; }
    }
}
