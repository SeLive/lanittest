﻿using CommunityToolkit.Mvvm.DependencyInjection;
using CommunityToolkit.Mvvm.Input;
using LanitTest.Interfaces;
using LanitTest.Models;
using LanitTest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Net.Mime.MediaTypeNames;

namespace LanitTest
{
    /// <summary>
    /// Логика взаимодействия для DetailWindow.xaml
    /// </summary>
    public partial class DetailWindow : Window
    {

        public DetailWindow(Employee employee)
        {
            InitializeComponent();
            
            DataContext = Ioc.Default.GetRequiredService<DetailWindowViewModel>();

            // Установка данных работника в форму для редактирования
            ViewModel.Employee = employee;
            ViewModel.CancelCommand = new RelayCommand(Close);
        }

        public DetailWindowViewModel ViewModel => (DetailWindowViewModel)DataContext;

        /// <summary>
        /// Разрешает вставку только чисел для TextBox
        /// </summary>
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[0-9]{1,}");

            if (!regex.IsMatch(e.Text))
            {
                e.Handled = true;
            }
        }
    }
}
