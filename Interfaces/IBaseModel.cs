﻿using System.Collections.Generic;

namespace LanitTest.Interfaces
{
    /// <summary>
    /// Интерфейс базовой модели
    /// </summary>
    public interface IBaseModel
    {
        Dictionary<string, string> GetAllAttributeHeaders();

        int GetPosition(string property);
    }
}
