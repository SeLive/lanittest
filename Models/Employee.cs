﻿using BLToolkit.Mapping;
using LanitTest.Attributes;

namespace LanitTest.Models
{
    /// <summary>
    /// Модель "Сотрудник"
    /// </summary>
    public class Employee : BaseModel
    {
        [MapField("JobTitle"), NotNull, DataGridColumnIngnore]
        public int JobTitleId { get; set; }

        [MapField("Subdivision"), NotNull, DataGridColumnIngnore]
        public int SubdivisionId { get; set; }

        [DataGridColumnIngnore]
        [Association(CanBeNull = false, ThisKey = "JobTitleId", OtherKey = "Id")]
        public JobTitle JobTitle { get; set; }

        [DataGridColumnIngnore]
        [Association(CanBeNull = false, ThisKey = "SubdivisionId", OtherKey = "Id")]
        public Subdivision Subdivision { get; set; }

        #region Свойтва для отоброжения
        [DataGridHeader("Оклад"), DataGridColimnPosition(4)]
        public long Sallary { get; set; }

        [DataGridHeader("ФИО"), DataGridColimnPosition(0)]
        public string FullName { get; set; }

        [DataGridHeader("Город"), DataGridColimnPosition(3)]
        public string Sity => Subdivision?.City?.Name;
        
        [DataGridHeader("Должность"), DataGridColimnPosition(1)]
        public string JobTitleName => JobTitle?.Name;

        [DataGridHeader("Подразделение"), DataGridColimnPosition(2)]
        public string SubdivisionName => Subdivision?.Name;
        #endregion
    }
}
