﻿using BLToolkit.Common;
using BLToolkit.DataAccess;
using LanitTest.Attributes;
using LanitTest.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LanitTest.Models
{
    /// <summary>
    /// Базовая модель
    /// </summary>
    public abstract class BaseModel : EntityBase, IBaseModel
    {
        /// <summary>
        /// ID записи
        /// </summary>
        [PrimaryKey(0), Identity, NonUpdatable]
        [DataGridColumnIngnore]
        public int Id { get; set; }

        /// <summary>
        /// Возвращает словарь заголовков для <see cref="System.Windows.Controls.DataGrid"/>, ключём является имя свойтва модели
        /// </summary>
        public Dictionary<string, string> GetAllAttributeHeaders()
        {
            var result = new Dictionary<string, string>();

            foreach (var prop in GetType().GetProperties())
            {
                var attributes = prop.GetCustomAttributes(typeof(DataGridHeaderAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    result.Add(prop.Name, (attributes.First() as DataGridHeaderAttribute).Title);
                }
            }

            return result;
        }

        /// <summary>
        /// Возвращает позицию из атрибута <see cref="DataGridColimnPositionAttribute"/> для указанного свойства. 
        /// В случае отсутвия атрибува вернётся 0
        /// </summary>
        /// <param name="property">Имя свойства модели</param>
        public int GetPosition(string property)
        {
            var prop = GetType().GetProperties().Where(x => x.Name == property).FirstOrDefault();

            if (prop != null)
            {
                var attributes = prop.GetCustomAttributes(typeof(DataGridColimnPositionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    return (attributes.First() as DataGridColimnPositionAttribute).Position;
                }
            }

            return 0;
        }

        /// <summary>
        /// Возвращает позицию из атрибута <see cref="DataGridColimnPositionAttribute"/> для указанного свойства. 
        /// В случае отсутвия атрибува вернётся 0
        /// </summary>
        /// <param name="property">Свойство модели</param>
        public int GetPosition(PropertyInfo propertyInfo)
        {
            return (propertyInfo.GetCustomAttributes(typeof(DataGridColimnPositionAttribute), false)?.FirstOrDefault() as DataGridColimnPositionAttribute)?.Position ?? 0;
        }

        /// <summary>
        /// Возвращает коллецию строк для записи в порядке заданным атрибутом <see cref="DataGridColimnPositionAttribute"/>.
        /// Если у модели не будет набора таких даннных, то вренётся пустая колекция.
        /// </summary>
        /// <returns>Коллеция строк для записи</returns>
        public List<string> GetExcelData()
        {
            var props = GetType().GetProperties()
                .Where(x => x.GetCustomAttributes().Any(y => y is DataGridColimnPositionAttribute)).ToList();

            if (props != null)
            {
                return props.OrderBy(x => GetPosition(x)).Select(x => x.GetValue(this)?.ToString()).ToList();
            }

            return new List<string>();
        }
    }
}
