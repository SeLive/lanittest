﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using LanitTest.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanitTest.Models
{
    /// <summary>
    /// Модель "Город"
    /// </summary>
    public class City : BaseModel
    {
        /// <summary>
        /// Название города
        /// </summary>
        public string Name { get; set; }

        public override string ToString() => Name;
    }
}
