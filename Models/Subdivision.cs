﻿using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using LanitTest.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanitTest.Models
{
    /// <summary>
    /// Модель "Подразделение"
    /// </summary>
    public class Subdivision : BaseModel
    {
        /// <summary>
        /// Название подразделения
        /// </summary>
        public string Name { get; set; }

        [MapField("City"), NotNull, DataGridColumnIngnore]
        public int CityId { get; set; }

        [Association(CanBeNull = false, ThisKey = "CityId", OtherKey = "Id"), DataGridColumnIngnore]
        public City City { get; set; }

        public override string ToString() => Name;
    }
}
