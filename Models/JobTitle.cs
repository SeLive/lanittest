﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanitTest.Models
{
    /// <summary>
    /// Модель "Должность"
    /// </summary>
    public class JobTitle : BaseModel
    {
        /// <summary>
        /// Название должности
        /// </summary>
        public string Name { get; set; }

        public override string ToString() => Name;
    }
}
