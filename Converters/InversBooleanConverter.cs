﻿using System;
using System.Windows;
using System.Windows.Data;

namespace LanitTest.Converters
{
    /// <summary>
    /// Конвертер <see cref="bool"/> значаний
    /// </summary>
    public class InversBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !System.Convert.ToBoolean(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
