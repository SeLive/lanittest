﻿using System;
using System.Windows.Data;
using System.Windows;

namespace LanitTest.Converters
{
    /// <summary>
    /// Конвертер <see cref="Visibility"/> значений
    /// </summary>
    public class InversVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((Visibility)value == Visibility.Hidden)
            { 
                return Visibility.Visible;
            }

            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
