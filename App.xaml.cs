﻿using CommunityToolkit.Mvvm.DependencyInjection;
using LanitTest.Services;
using LanitTest.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using OfficeOpenXml;
using System.Windows;

namespace LanitTest
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            // Глобальный перехватчик всех не перехваченых исключений
            DispatcherUnhandledException += (s, e) => {
                MessageBox.Show($"{e.Exception.Message}\n\n{e.Exception.StackTrace}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            };

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            InitializeComponent();            

            // Настройка сервисов
            Ioc.Default.ConfigureServices(
                new ServiceCollection()
                .AddTransient<ContextDB>()
                .AddTransient<MainWindowViewModel>()
                .AddTransient<DetailWindowViewModel>()
                .BuildServiceProvider());
        }
    }
}
