﻿using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.Data.Linq;
using LanitTest.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanitTest.Services
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    public class ContextDB : DbManager
    {
        public Table<City> City => GetTable<City>();
        public Table<Subdivision> Subdivision => GetTable<Subdivision>();
        public Table<JobTitle> JobTitle => GetTable<JobTitle>();
        public Table<Employee> Employee => GetTable<Employee>();

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        public ContextDB() : base(new OdpManagedDataProvider(), ConfigurationManager.AppSettings["ConnectionString"]) { }

        /// <summary>
        /// Производит обновление сотрудника
        /// </summary>
        /// <param name="employee">Сотрудник</param>
        public void UpdateEmployee(Employee employee)
        {
            Employee.Where(x => x.Id == employee.Id)
                .Set(x => x.JobTitleId, employee.JobTitle.Id)
                .Set(x => x.SubdivisionId, employee.Subdivision.Id)
                .Set(x => x.Sallary, employee.Sallary)
                .Set(x => x.FullName, employee.FullName)
                .Update();
        }

        /// <summary>
        /// Возвращает список работников используя фильтр
        /// </summary>
        /// <param name="filter">Фильтр</param>
        public IQueryable<Employee> GetEmployeesByFilter(string filter)
        {
            var query = Employee.AsQueryable();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                query = query.Where(x =>
                    x.FullName.ToLower().Contains(filter) ||
                    x.Subdivision.Name.ToLower().Contains(filter) ||
                    x.Subdivision.City.Name.ToLower().Contains(filter));
            }

            return query.Select(x => new Employee()
            {
                FullName = x.FullName,
                JobTitle = x.JobTitle,
                Id = x.Id,
                JobTitleId = x.JobTitleId,
                Sallary = x.Sallary,
                Subdivision = new Subdivision
                {
                    CityId = x.Subdivision.CityId,
                    Id = x.Subdivision.Id,
                    Name = x.Subdivision.Name,
                    City = new City()
                    {
                        Id = x.Subdivision.City.Id,
                        Name = x.Subdivision.City.Name,
                    }
                }
            });
        }
    }
}
