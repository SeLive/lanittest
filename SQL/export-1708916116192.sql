INSERT INTO ADMIN.CITY (ID,NAME) VALUES
	 (1,'Москва'),
	 (2,'Ростов-на-Дону'),
	 (3,'Новороссийск'),
	 (21,'Хабаровск');
INSERT INTO ADMIN.EMPLOYEE (ID,FULLNAME,JOBTITLE,SALLARY,SUBDIVISION) VALUES
	 (1,'Власов Дмитрий Андреевич',1,140000,1),
	 (2,'Александр Сергеевич Петров',1,120000,24),
	 (3,'Иван Иванович Сидоров',3,300000,1),
	 (4,'Мария Александровна Кузнецова',4,70000,23);
INSERT INTO ADMIN.JOBTITLE (ID,NAME) VALUES
	 (1,'Разработчик'),
	 (2,'Тестировщик'),
	 (3,'Начальник'),
	 (4,'HR-менеджер');
INSERT INTO ADMIN.SUBDIVISION (ID,NAME,CITY) VALUES
	 (1,'Начальство',1),
	 (21,'Разработчики',2),
	 (23,'HR',3),
	 (24,'Разработчики 2',1);
