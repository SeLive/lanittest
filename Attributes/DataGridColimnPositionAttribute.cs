﻿using System;

namespace LanitTest.Attributes
{
    /// <summary>
    /// Атрибут установка положения колонки в <see cref="System.Windows.Controls.DataGrid"/>
    /// </summary>
    public class DataGridColimnPositionAttribute : Attribute
    {
        /// <summary>
        /// Позиция
        /// </summary>
        public int Position { get; }

        /// <summary>
        /// Установка положения колонки в <see cref="System.Windows.Controls.DataGrid"/>
        /// </summary>
        /// <param name="position">Позиция</param>
        public DataGridColimnPositionAttribute(int position)
        {
            Position = position;
        }
    }
}
