﻿using System;

namespace LanitTest.Attributes
{
    /// <summary>
    /// Атрибут угнорирования колонки в <see cref="System.Windows.Controls.DataGrid"/>
    /// </summary>
    public class DataGridColumnIngnoreAttribute : Attribute { }
}
