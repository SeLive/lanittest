﻿using System;

namespace LanitTest.Attributes
{
    /// <summary>
    /// Атрибут имени колонки в <see cref="System.Windows.Controls.DataGrid"/>
    /// </summary>
    public class DataGridHeaderAttribute : Attribute
    {
        /// <summary>
        /// Имя колонки
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Задаёт имя колонки в <see cref="System.Windows.Controls.DataGrid"/>
        /// </summary>
        /// <param name="title">Имя</param>
        public DataGridHeaderAttribute(string title)
        {
            Title = title;
        }
    }
}
