# LanitTest
## Тестовое задание

### Установка и настройка
Устанавливаем Oracle 23c 

```sh
docker pull container-registry.oracle.com/database/free:latest
```

Далее первичная настройка чистой БД. Авторизуемся через `bash` в докере.

```sh
sqlplus / as sysdba
```
И создаём нашего пользователя
```sql
alter session set "_ORACLE_SCRIPT"=true;
create user admin identified by admin;
grant all privileges to admin;
```

Теперь заливаем БД из `./SQL/export-1708916116192.sql`

Настройки приложения хранятся в App.config

Теперь можно запускать. Не забываем подтянуть все зависимости из Nuget